#!/usr/bin/env python3.8

"""
 Copyright (C) 2019  PhotonQyv

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import dbus
import logging
import sys
import gi.repository.GLib

from urllib import parse
from dbus.mainloop.glib import DBusGMainLoop
from mastodon import Mastodon, MastodonError
from pydbus import SessionBus, Variant, bus
from argparse import ArgumentParser

# Get Clementine D-Bus object
def get_clem() -> bus.Bus:
    return session_bus.get("org.mpris.MediaPlayer2.clementine", "/org/mpris/MediaPlayer2")


# Get Notifications D-Bus object
def get_notifications() -> bus.Bus:
    return session_bus.get(".Notifications", "/org/freedesktop/Notifications")


# Callback that checks for Notifications from Clementine
def do_check(bus: bus.Bus, message: dbus.lowlevel.Message):
    args_list = message.get_args_list()

    client, paused_stopped = args_list[0], args_list[4]

    member = message.get_member()

    if paused_stopped == "Stopped" or paused_stopped == "Paused":

        logging.debug(f"{paused_stopped=}")

    else:

        logging.debug("DO TOOT")
        do_toot()


# Method that does the heavy lifting
def do_toot():
    clem = get_clem()
    notifications = get_notifications()

    data = clem.Metadata

    if (
            not "xesam:artist" in data.keys()
            or not "xesam:title" in data.keys()
            or not "xesam:album" in data.keys()
    ):
        sys.exit(1)

    artist, *_ = data["xesam:artist"]
    title = data["xesam:title"]
    album = data["xesam:album"]

    art_url = ""

    # Not every album has a cover image
    if "mpris:artUrl" in data.keys():

        art_url = data["mpris:artUrl"]
        art_file = parse.urlparse(art_url).path

    else:

        art_file = ""
        art_url = ""

    logging.debug(f"{art_file=}")

    output = f"#NowPlaying\n\n{title}\n\n{artist}\n\n{album}\n\n#nowplaying #Clementine"

    output = output.encode("utf8")

    logging.debug(f"{output=}")

    send_post(album, artist, art_file, output)

    hints = {}

    if art_url != "":
        # hints in Notify() is a dictionary with
        #  a str key and Variant value (in this case a str).
        hints = {"image-path": Variant.new_string(art_url)}

    else:

        hints = {}

    try:

        notifications.Notify(
            "Clementine Tooter",
            0,
            "",
            "#NowPlaying...",
            f"Artist: {artist}\nTitle: {title}\nAlbum: {album}",
            None,
            hints,
            5000,
            )

    except Exception as e:

        logging.error(f"{e=}")


# Send the Mastodon Status Update.
def send_post(album: str, artist: str, artfile: str, output: bytes):

    try:

        mastodon = Mastodon(
            access_token="/home/qyv/Documents/Important/np_clementine-keys/np_clementine_usercred.secret",
            api_base_url="https://mastodon.xyz/",
            )

        if artfile:

            logging.debug(f"WE HAVE ART: {artfile}")

            result = mastodon.media_post(
                artfile, description=f"Cover image of '{album}' by {artist}"
                )

            mastodon.status_post(
                output, visibility="unlisted", language="eng", media_ids=result
                )

        else:

            logging.debug("WE DON'T")

            mastodon.status_post(output, visibility="unlisted", language="eng")

    except MastodonError as me:

        logging.error(f"{me=}")


if __name__ == "__main__":

    mainloop = None

    # Default to INFO level of logging
    log_level = logging.INFO

    parser = ArgumentParser()

    parser.description = "Post what's playing in Clementine to Mastodon."

    parser.add_argument("-d", "--debug", help="turn debugging on", action="store_true")

    args = parser.parse_args()

    if args.debug:

        log_level = logging.DEBUG

    else:

        log_level = logging.INFO

    logging.basicConfig(level=log_level, format="%(message)s")

    try:

        DBusGMainLoop(set_as_default=True)

        session_bus = (
            SessionBus()
            )  # Use pydbus version for everything since it's easier to work with.

        # Use dbus-python for the listening for notifications,
        #  since I've not yet worked out how to do the eavesdropping in pydbus.
        note_bus = dbus.SessionBus()

        note_bus.add_match_string_non_blocking(
            "eavesdrop=true, interface='org.freedesktop.Notifications', member='Notify', arg0='Clementine'"
            )

        note_bus.add_message_filter(do_check)

        mainloop = gi.repository.GLib.MainLoop()
        mainloop.run()

    except KeyboardInterrupt:

        print()
        mainloop.quit()
